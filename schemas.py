from pydantic import BaseModel
from typing import List, Optional
import datetime


identification = 0  # potrebno uvecati nakon svakog novog ispita


class IspitIn(BaseModel):
    id: int = identification  # identifikacioni broj ispita, potrebno ukloniti
    ispit_godina: int
    ispit_mjesec: int
    ispit_dan: int
    ispit_sat: int
    ispit_minute: int
    mjesto: str
    isPolozen: bool = False
    isPrijavljen: bool = False
    isPrisustvovao: bool = False
    bodovi: int = 50
    vidljiv: bool = True  # promjenjivu vidljiv koristim za soft delete, u slucaju brisanja,
    # setuje se samo vidljiv na false


class PredmetIn(BaseModel):
    naziv: str
    profesor: str
    asistent: str
    predavanja_mjesto: str
    predavanje_godina: int
    predavanje_mjesec: int
    prevanje_dan: int
    predavanje_sat: int
    predavanje_minute: int
    vjezbe_mjesto: str
    vjezbe_godina: int
    vjezbe_mjesec: int
    vjezbe_dan: int
    vjesbe_sat: int
    vjezbe_minute: int
    lab_mjesto: str
    lab_vrijeme: datetime.datetime
    ispiti: List[Optional[IspitIn]] = None


class IspitOut(BaseModel):
    isPolozen: bool


class PredmetOut(BaseModel):
    naziv: str
    profesor: str
    ispiti: IspitOut